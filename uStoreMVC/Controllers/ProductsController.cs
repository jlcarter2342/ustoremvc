﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Mvc;

namespace uStoreMVC.Models
{
    public class ProductsController : Controller
    {
        private string connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        // GET: Products
        public ActionResult GetProductNames()
        {
            List<ProductsModel> products = new List<ProductsModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=.\sqlexpress; Initial Catalog=uStore; Integrated Security=true";

                conn.Open();

                SqlCommand cmdGetProducts = new SqlCommand("select * from products", conn);

                SqlDataReader rdrProducts = cmdGetProducts.ExecuteReader();

                string text = "";

                while (rdrProducts.Read())
                {
                    ProductsModel prod = new ProductsModel();
                    {
                        prod.ProductID = (int) rdrProducts["ProductID"];
                        prod.ProductName = (string) rdrProducts["ProductName"];
                        prod.Price = (decimal) rdrProducts["Price"];
                        prod.ProductStatusID = (byte) rdrProducts["ProductStatusID"];
                        prod.UnitsInStock = (Int16) rdrProducts["UnitsInStock"];

                        prod.ProductDescription = (rdrProducts["ProductDescription"] is DBNull) ? "N/A" : (string) rdrProducts["ProductDescription"];
                    }

                    products.Add(prod);
                }

                rdrProducts.Close();
                conn.Close();
            }


            return View(products);
        }
    }
}