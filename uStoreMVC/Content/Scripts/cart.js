﻿//JavaScript Object to store book information
var books = [
    {
        id: 1,
        title: "The Eye of Minds",
        author: "James Dashner",
        price: 9.99
    },
    {
        id: 2,
        title: "Harold and the Purple Crayon",
        author: "Crockett Johnson",
        price: 6.95
    },
    {
        id: 3,
        title: "Essential C# 6.0",
        author: "Mark Michaelis & Eric Lippert",
        price: 46.99
    },
    {
        id: 4,
        title: "The Maze Runner",
        author: "James Dashner",
        price: 7.65
    },
    {
        id: 5,
        title: "Macbeth",
        author: "William Shakespeare",
        price: 6.50
    },
    {
        id: 6,
        title: "Triumph",
        author: "Jeremy Schaap",
        price: 11.45
    }
];



//Create an array to store user's cart info
var cart = [];

//Add items to cart -- wired to <a> tags in the HTML
function addToCart(id) {
    //If they have not added any of the titles yet, set the qty to 1
    //and add the book to our array
    //Otherwise, add 1 to the qty
    var bookObj = books[id - 1];
    if (typeof bookObj.qty === 'undefined') {
        bookObj.qty = 1;
        cart.push(bookObj);
    }
    else {
        bookObj.qty = bookObj.qty + 1;
    }


    /* For testing purposes */
    console.clear();
    var arrayLength = cart.length;
    for (var i = 0; i < arrayLength; i++) {
        console.log(cart[i]);
    }

    document.getElementById('cart-notification').style.display = 'block';

    //Total number of books in the cart
    var cartQty = 0;
    for (var i = 0; i < arrayLength; i++) {
        cartQty += cart[i].qty;
    }

    document.getElementById('cart-notification').innerHTML = cartQty;


    document.getElementById('cart-contents').innerHTML = getCartContents();

}


//Show the user which books they have added to the cart, the qty, 
//price per book and total price of all books
function getCartContents() {
    var cartContent = "";
    var cartTotal = 0;

    for (var i = 0; i < cart.length; i++) {
        cartContent += cart[i].title + "<br>by " + cart[i].author + "<br>Qty: " +
            cart[i].qty + " at " + cart[i].price.toFixed(2) + " ea.<br><br>";

        cartTotal += cart[i].qty * cart[i].price;
    }

    cartContent += "Cart Total: $" + cartTotal.toFixed(2);

    return cartContent;
}