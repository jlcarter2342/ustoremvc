﻿function validateForm() {
    //With custom JS validation we will require each field
    var name = document.forms['main-contact-form']['name'].value;
    var email = document.forms['main-contact-form']['email'].value;
    var subject = document.forms['main-contact-form']['subject'].value;
    var message = document.forms['main-contact-form']['message'].value;

    //Get the error message <span> tags
    var nameVal = document.getElementById('nameVal');
    var emailVal = document.getElementById('emailVal');
    var subjectVal = document.getElementById('subjectVal');
    var messageVal = document.getElementById('messageVal');

    //Declare Regular Expression Object for email
    var emailRegEx = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);


    //MINI LAB!
    //Use a regular expression to verify that first names only include 
    //alphabetical characters
    var nameRegEx = new RegExp(/^[A-z]+$/);


    //Test all of our conditions including checking for a valid email
    if (name.length == 0 || email.length == 0 || subject.length == 0 || message.length == 0
        || !emailRegEx.test(email) || !nameRegEx(name)) {

        //Error message for required fields
        if (name.length == 0) {
            nameVal.textContent = "* Name is required";
        }
        else {
            nameVal.textContent = "";
        }

        if (email.length == 0) {
            emailVal.textContent = "* Email is required";
        }
        else {
            emailVal.textContent = "";
        }

        if (subject.length == 0) {
            subjectVal.textContent = "* Subject is required";
        }
        else {
            subjectVal.textContent = "";
        }

        if (message.length == 0) {
            messageVal.textContent = "* Message is required";
        }
        else {
            messageVal.textContent = "";
        }

        //Error message if email is not valid
        if (!emailRegEx.test(email) && email.length > 0) {
            emailVal.textContent = "* Must be a valid email address";
        }

        if (emailRegEx.test(email) && email.length > 0) {
            emailVal.textContent = "";
        }

        //Error message if name contains numbers
        if (!nameRegEx.test(name) && name.length > 0) {
            nameVal.textContent = "* Name can only contain alphabetical characters";
        }

        if (nameRegEx.test(name) && name.length > 0) {
            nameVal.textContent = "";
        }
        event.preventDefault();
    }
}