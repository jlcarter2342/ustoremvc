﻿using System;

namespace uStoreMVC.Models
{
    public class ProductsModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public decimal Price { get; set; }
        public Int16 UnitsInStock { get; set; }
        public byte ProductStatusID { get; set; }
    }
}